<?php
namespace App\BookTitle;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;

class BookTitle extends DB{
    public $id;
    public $book_title;
    public $author_name;
    public $publish_date;
    public function __construct()
    {
        parent::__construct();
    }

    public function  index(){

        echo "i am inside the index method of booktitle class<br>";

    }


    public function setdata($postVariableData=NULL){

            if(array_key_exists("id",$postVariableData))
            {
                $this->id= $postVariableData['id'];
            }

        if(array_key_exists("book_title",$postVariableData))
        {
            $this->book_title= $postVariableData['book_title'];
        }
        if(array_key_exists("author_name",$postVariableData))
        {
            $this->author_name= $postVariableData['author_name'];
        }


    }//end of setdata()
    public function store(){
        $arrdata=array($this->book_title,$this->author_name);
                $sql="insert into book_title(book_title,author_name) VALUES (?,?)";
        $STH=    $this->DBH->prepare($sql);
       $result= $STH->execute($arrdata);
            if($result)
        Message::message("Success!Data Has Been Inserted Successfully :)");
        else
            Message::message("Failed!Data Hasn't Been Inserted Successfully (:");
        Utility::redirect('create.php');

    }//end of store method

}//end of booktitle class