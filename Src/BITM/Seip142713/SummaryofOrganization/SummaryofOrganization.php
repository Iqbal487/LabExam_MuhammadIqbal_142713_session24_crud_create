<?php
namespace App\SummaryofOrganization;
//use App\Gender\Gender;
use App\Message\Message;
use App\Model\Database as DB;

use App\Utility\Utility;

class SummaryofOrganization extends DB{


    public $id;
    public $name;
    public $organizationname;
    public $Position;
    public function __construct()
    {
        parent::__construct();

    }
    public function index()
    {

        echo "i am inside the SummaryofOrganization class";

    }
    public function setdata($postVariableData = NULL)
    {


        if (array_key_exists("name", $postVariableData)) {
            $this->name = $postVariableData['name'];
        }
        if (array_key_exists("organizationname", $postVariableData)) {
            $this->organizationname = $postVariableData['organizationname'];
        }
        if (array_key_exists("Position", $postVariableData)) {
            $this->Position = $postVariableData['Position'];
        }


    }

//end of setdata()
    public function store()
    {
        $arrdata = array($this->name,$this->organizationname,$this->Position);
        $sql = "insert into summary_of_organization(name,organizationname,Position) VALUES (?,?,?)";
        $STH = $this->DBH->prepare($sql);
        $result = $STH->execute($arrdata);
        if ($result)
            Message::message("Success!Data Has Been Inserted Successfully :)");
        else
            Message::message("Failed!Data Hasn't Been Inserted Successfully (:");
        Utility::redirect('create.php');

    }//end of store method

}