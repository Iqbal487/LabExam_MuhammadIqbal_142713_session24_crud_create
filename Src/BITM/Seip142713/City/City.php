<?php
namespace App\City;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;

class City extends DB
{


    public $id;
    public $name;
    public $cityname;

    public function __construct()
    {

        parent::__construct();
    }

    public function index()
    {


        echo "i am inside the city class.<br> ";

    }


    public function setdata($postVariableData = NULL)
    {


        if (array_key_exists("name", $postVariableData)) {
            $this->name = $postVariableData['name'];
        }
        if (array_key_exists("cityname", $postVariableData)) {
            $this->cityname = $postVariableData['cityname'];
        }


    }

//end of setdata()
    public function store()
    {
        $arrdata = array($this->name, $this->cityname);
        $sql = "insert into city(name,cityname) VALUES (?,?)";
        $STH = $this->DBH->prepare($sql);
        $result = $STH->execute($arrdata);
        if ($result)
            Message::message("Success!Data Has Been Inserted Successfully :)");
        else
            Message::message("Failed!Data Hasn't Been Inserted Successfully (:");
        Utility::redirect('create.php');

    }//end of store method

}