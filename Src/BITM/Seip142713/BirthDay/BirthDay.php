<?php

namespace App\BirthDay;
use App\Message\Message;
 use App\Model\Database as DB;
use App\Utility\Utility;
class BirthDay extends DB{


    public $id;
    public $name;
    public $date;

    public function __construct()
    {
 parent::__construct();
    }
    public function index(){


        echo "i am inside the Birthday Class.<br> ";
    }
    public function setdata($postVariableData=NULL){



        if(array_key_exists("name",$postVariableData))
        {
            $this->name= $postVariableData['name'];
        }
        if(array_key_exists("date",$postVariableData))
        {
            $this->date= $postVariableData['date'];
        }


    }//end of setdata()
    public function store(){
        $arrdata=array($this->name,$this->date);
        $sql="insert into birthday(name,date) VALUES (?,?)";
        $STH=    $this->DBH->prepare($sql);
        $result= $STH->execute($arrdata);
        if($result)
            Message::message("Success!Data Has Been Inserted Successfully :)");
        else
            Message::message("Failed!Data Hasn't Been Inserted Successfully (:");
        Utility::redirect('create.php');

    }//end of store method

//end of booktitle class


}


