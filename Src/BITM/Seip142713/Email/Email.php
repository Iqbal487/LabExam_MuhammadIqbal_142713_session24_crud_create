<?php

namespace App\Email;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;


class Email extends DB{


    public $id;
    public $name;
    public $email;
    public function __construct()
    {
        parent::__construct();

    }
    public function index()
    {

        echo "i am inside the email class";

    }
    public function setdata($postVariableData = NULL)
    {


        if (array_key_exists("name", $postVariableData)) {
            $this->name = $postVariableData['name'];
        }
        if (array_key_exists("email", $postVariableData)) {
            $this->email = $postVariableData['email'];
        }


    }

//end of setdata()
    public function store()
    {
        $arrdata = array($this->name, $this->email);
        $sql = "insert into email(name,email) VALUES (?,?)";
        $STH = $this->DBH->prepare($sql);
        $result = $STH->execute($arrdata);
        if ($result)
            Message::message("Success!Data Has Been Inserted Successfully :)");
        else
            Message::message("Failed!Data Hasn't Been Inserted Successfully (:");
        Utility::redirect('create.php');

    }//end of store method


}