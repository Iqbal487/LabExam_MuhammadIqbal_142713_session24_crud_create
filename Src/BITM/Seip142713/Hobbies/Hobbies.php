<?php
namespace App\Hobbies;
//use App\Gender\Gender;
use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;


class Hobbies extends DB{


    public $id;
    public $name;
    public $hobbyname;
    public function __construct()
    {
        parent::__construct();

    }
    public function index()
    {

        echo "i am inside the hobby class";

    }
    public function setdata($postVariableData = NULL)
    {


        if (array_key_exists("name", $postVariableData)) {
            $this->name = $postVariableData['name'];
        }
        if (array_key_exists("hobbyname", $postVariableData)) {
            $this->hobbyname = implode(',',$postVariableData['hobbyname']);
        }


    }

//end of setdata()
    public function store()
    {
        $arrdata = array($this->name, $this->hobbyname);
        $sql = "insert into hobbies(name,hobbyname) VALUES (?,?)";
        $STH = $this->DBH->prepare($sql);
        $result = $STH->execute($arrdata);
        if ($result)
            Message::message("Success!Data Has Been Inserted Successfully :)");
        else
            Message::message("Failed!Data Hasn't Been Inserted Successfully (:");
        Utility::redirect('create.php');

    }//end of store method


}